# Contributions

_Contenu :_ Règles du jeu HellTaker

_Auteurs :_ François Marechal, Maxime Delboulle, Samar Mellouki

_Date de création :_ 10 mai 2022

_Date de dernière modification :_ 19 mai 2022

# Règles du Jeu

## Situation Initiale

**Règle 1 : Position initiale du personnage**
> Initialement, le personnage est positionné sur une carte, il n'a aucune clé et dispose d'un certain nombre de pas (généralement, ce nombre de pas correspond à la distance pour atteindre la solution.

Conséquence :
- Compteur de clé à 0
- Compteur de pas à h (heuristique)
- Position du joueur (x, y)

**Règle 2 : Position initiale du but**
> Le personnage doit atteindre une case adjacente à la cible placée sur la carte. Il n'y a qu'une seule cible par carte mais elle peut avoir une taille variable (ex : Cerberus de taille 3 mais rochers autour de toutes manières.)

Conséquence :
- Position de la cible (x2, y2)

**Règle 3 : Limites fixes de la carte**
> La carte est limitée par des murs et des rochers immobiles.

Conséquence :
- Le quadrillage est delimité par les murs et les rochers.

**Règle 4 : Items mobiles de la carte**

> Des ennemis peuvent être positionnés sur la carte.

> Des blocs peuvent être positionnés sur la carte.

> Un coffre peut être positionné sur la carte.

> Des pics peuvent être positionnés sur la carte.

> Une clé peut être positionnée sur la carte.

Conséquence :
- Position des ennemis [(x, y), (x, y)]
- Position des blocs [(x, y), (x, y)]
- Position des pics [(x, y), (x, y)]
- Position du coffre (x, y)
- Position de la clé (x, y)

**Règle 5 : Particularité des pics**

> Initialement, les pics sont sont dans un état déterminé ("relevé" ou "caché"/"non-relevé") et non-uniforme (tous les pics n'ont pas le même état au début de la partie). 

Conséquence :
- Position des pics [ { pos: (x, y), relevé : True }, { pos : (x, y), relevé : False }]

> A chaque mouvement, leur état s'inverse.

Conséquence :
- Mettre à jour l'état des pics à chaque pas.


## Actions : Déplacements sur la carte

Les actions du joueur à chaque tour sont soit le mouvement ou soit le déplacement d'objets (la tentative du moins).

**Règle 6 : [ACTION 1 : Mouvement] Conditions de déplacement**

> Le joueur peut se déplacer dans 4 directions (haut, bas, gauche, droite) uniquement sur des cases libres et atteignables.

> Une case est atteignable si elle se situe sur la carte à 1 pas du joueur.

> Une case est libre s'il n'y a aucun ennemi, bloc, mur/rocher ou coffre sur la case.

> Si le personnage se déplace sur une case contenant une clef, alors il prend possession de la clef.

> Une case contenant un coffre devient libre si le joueur dispose d'une clé.

Conséquence :
- Le joueur peut se déplacer en haut, à droite, en bas, à gauche.
- Case atteignable : à 1 pas
- Case Libre : vide, clé, pic - et coffre si clé

**Règle 7 : [ACTION 2 : Poussée] Conditions de poussée**


> Un joueur peut tenter de pousser un bloc ou un ennemi dans la même direction que le mouvement initié (vers le haut, le bas, la gauche, la droite)

> Si la case vers laquelle l'ennemi est poussé n'est pas libre (bloc, mur), alors il est supprimé. Sinon, il sera déplacé vers la case libre.

> Si la nouvelle position de l'ennemi contient des pics, il sera supprimé. Cette règle simplifie la véritable règle du jeu imposant la suppression si les pics sont relevés. Néanmoins, si le joueur pousse un ennemi, alors il se trouve à au moins deux pas de ce dernier et ne peut donc plus interagir avec lui, car soit les pics sont déjà relevés, soit ils seront relevés au tour d'après. On peut donc considérer un ennemi positionné sur des pics à une distance supérieure à deux pas du joueur comme supprimé.

> Si la case vers laquelle un bloc est poussée n'est pas libre, alors le bloc reste à sa place. Sinon, le bloc est deplacé vers la case libre (pas d'effet des pics ou des clés sur le bloc).

Conséquence :
- Les ennemis peuvent disparaitre s'ils sont poussés vers des murs, des blocs ou sur des pics, sinon ils sont déplacés.
- Les blocs restent immobiles si la case de destination n'est pas libre, sinon ils sont déplacés.


**Règle 8 : Coût d'une action**

> Pousser un ennemi ou un bloc coûte 1.

> Se déplacer sur une case libre et sans pics relevés coûte 1.

> Se déplacer sur une case libre avec pics relevés coûte 2.

Conséquence :
- Nombre de pas se décrémente de 1 ou 2 pas en fonction de l'action


## Fin du Jeu
> Si le compteur de pas du joueur vaut 0 et qu'il n'est pas à côté de la cible, alors il a perdu.

> Dans le cas contraire, il gagne (case adjacente à la cible).

Conséquence : 
- Fin du jeu 
- Game Over si nombre de pas atteint
- Win si le joueur est sur une case adjacente à la cible.
