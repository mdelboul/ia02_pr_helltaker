# Contributions

_Contenu :_ Modélisation des règles du jeu HellTaker avec STRIPS

_Auteurs :_ François Marechal, Maxime Delboulle, Samar Mellouki

_Date de création :_ 10 mai 2022

_Date de dernière modification :_ 19 mai 2022

TODO : Modifier Empty car ce n'est pas un bon choix de prédicats niveau cohérence (avec Block(b) On(cell, b) cell(cell) la c'est pas empty mais si j'oublie le non empty ca fausse tout :/ jsp ce que vous en pensez. On gère nous façon le empty mais ca fait un truc a maintenir en double

Après si je le retire, ca veut dire que faut verifier que y a ni bloc et tout et c super galere aussi, surtout à cause du coffre :/)

# STRIPS - Proposition de Samar

## Prédicats du monde

On commence par représenter le monde. Les éléments basique du monde sont les suivants :
```cpp
Player(x) // x est le joueur

Target(x) //x est la cible

Cell(c) // c est une case (ni un mur ni un rocher)

Spikes(x) // x est un piège (pics)
```

## Fluents

Les fluents sont des prédicats qui ne sont vrais qu'à un moment donné.

Ensuite, les éléments du monde peuvent avoir certaines propriétés notamment une position (x, y), un état (relevé ou non, possède la clé ou non). On les représentera de la manière suivante :
```cpp
SpikesOn(x) // Il y a des pics sur x
PlayerOn(x) // Le joueur est sur x

CofferOn(x) // Il y a un coffre sur x
KeyOn(x) // Il y a une clé sur x
EnnemyOn(x) // Il y a un ennemi sur x
BlockOn(x) // Il y a un bloc sur x

Near(x, y) // x est adjacent à la cible
PlayerHasKey() // le joueur a une clé
```

## Etat Initial

On va prendre pour illustration le niveau 3 du jeu Helltaker.

![alt text](level3.png "Image originale")

Nous la numéroterons comme suit :

![alt text](level3_cells.png "Image modifiée")

_Note : B1 est une erreur sur l'image..._

Les prédicats/fluents non mentionnés sont considérés comme faux.

STRIPS ne prenant pas les égalités (=), nous avons positionner les cases relativement aux autres avec un prédicat `Near`

L'état initial de cette carte est alors le suivant :

```cpp
Init(
      Cell(A0)
    ∧ Cell(B0) ∧ KeyOn(B0)
    ∧ Cell(A1)
    ∧ ...
    ∧ Cell(F6) ∧ CoffreOn(F6)
    ∧ ...

    //Positionner les cases relativement aux autres 
    ∧ Near(B0, A0) ∧ Near(A0, B0)
    ∧ Near(A0, A1) ∧ Near(A0, A1)
    ∧ ... // toutes les autres relations

    // Position du player
    ∧ PlayerOn(E7)

    //Positions des pics
    ∧ SpikesOn(B2) ∧ SpikesOn(D2) ∧ SpikesOn(E3) ∧ ... //tous les autres pics

    // Positions des ennemis
    ∧ EnnemyOn(A5) ∧ EnnemyOn(C4)

    ∧ Target(H6)
)
```

## But

L'objectif du jeu est de positionner le joueur sur une case adjacente à la cible.

```cpp
Goal(
    PlayerOn(cible) ∧ Cell(cible) ∧ Target(cible)
)
```

## Actions

On considère les actions suivantes :
- Déplacer le personnage sur une case vide
- Déplacer le personnage sur une case avec une clé
- Déplacer le personnage sur une case avec un coffre après avoir récupéré la clé
- Pousser un monstre vers une case vide
- Pousser un monstre vers un mur
- Pousser un monstre vers un bloc
- Pousser un bloc

### Déplacer le personnage sur case vide

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers une autre case libre adjacente.

```cpp
Action(
    MovePayer(caseMe, caseDest),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action sur case atteignable et différente
        ∧ Cell(caseDest) ∧ Near(caseMe, caseDest) ∧ (caseDest ≠ caseMe) 

        // La case dest est vide
        ∧ ¬KeyOn(caseDest) ∧ ¬CofferOn(caseDest) ∧ ¬EnnemyOn(caseDest) ∧ ¬BlockOn(caseDest)

    EFFECT: 
        //Le joueur n'est plus sur la caseMe mais sur la caseDest
        ¬PlayerOn(caseMe) ∧ PlayerOn(caseDest)
)
```

### Se déplacer sur une case avec une clé

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers une autre case avec une clé.

```cpp
Action(
    MoveAndGetKey(caseMe, caseKey),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action demandée sur case atteignable et différente
        ∧ Cell(caseKey) ∧ Near(caseKey, caseMe) ∧ (caseKey ≠ caseMe)

        // Il y a une clé sur la case
        ∧ KeyOn(caseKey),

    EFFECT: 
        //Le joueur passe de la caseMe à la caseKey et il a la clé
        ¬PlayerOn(caseMe) ∧ PlayerOn(caseKey)

        // La clé n'est plus sur la case car le joueur a la clé
        ∧ ¬KeyOn(caseKey) ∧ PlayerHasKey()
)
```

### Se déplacer sur une case avec un coffre en ayant la clé

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers un coffre après avoir trouvé la clé.

```cpp
Action(
    MoveAndOpenCoffer(caseMe, caseCoffre),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action demandée sur case atteignable et différente
        ∧ Cell(caseCoffre) ∧ Near(caseMe, caseCoffre) ∧ (caseCoffre ≠ caseMe) 

        // Il y a un coffre sur la case
        ∧ CofferOn(caseCoffre) 

        // Le joueur a la clé
        ∧ PlayerHasKey(), 

    EFFECT: 
        //Le coffre n'est plus sur la case coffre
        ¬CofferOn(caseCoffre)

        // Le joueur s'est déplacé sur la case coffre
        ¬PlayerOn(caseMe) ∧ PlayerOn(caseCoffre)
        
        // Le joueur n'a plus la clé
        ∧ ¬PlayerHasKey()
)
``` 


### Pousser un ennemi vers une case vide

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers un monstre donc il le pousse vers la 3e case dans la même direction et celle-ci est libre

```cpp
Action(
    PushMonster(caseMe, caseMonstre, caseDest),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action sur case atteignable et différente
        ∧ Cell(caseMonstre) ∧ Near(caseMe, caseMonstre) ∧ (caseMonstre ≠ caseMe) 

        //Il y a un monstre sur la case
        ∧ EnnemyOn(caseMonstre)

        // La 3e case est libre pour le monstre //todo : est-ce que le monstre peut etre sur une clé ? Là, j'ai mis oui
        ∧ Cell(caseDest) ∧ ¬CofferOn(caseDest) ∧ ¬EnnemyOn(caseDest) ∧ ¬BlockOn(caseDest),

    EFFECT: 
        //Le monstre n'est plus sur la case monstre mais sur la case dest
        ¬EnnemyOn(caseMonstre) ∧ EnnemyOn(monstre, caseDest)
)
``` 

### Pousser un ennemi sur un mur (l'éliminer)

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers un monstre donc il le pousse vers un mur ce qui provoque son élimination.

```cpp
Action(
    PushOnWallAndKillMonster(caseMe, caseMonstre, caseDest),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action sur case atteignable et différente
        ∧ Cell(caseMonstre) ∧ Near(caseMe, caseMonstre) ∧ (caseMonstre ≠ caseMe) 

        //Il y a un monstre sur la case
        ∧ EnnemyOn(caseMonstre) 

        // La 3e case n'est pas une case
        ∧ ¬Cell(caseDest),

    EFFECT: 
        //Le monstre n'est plus sur la case monstre et n'existe plus
        ¬EnnemyOn(monstre, caseMonstre)
)
``` 

### Pousser un ennemi sur un block (l'éliminer)

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers un monstre donc il le pousse vers un mur ce qui provoque son élimination.

```cpp
Action(
    PushOnBlocksAndKillMonster(caseMe, caseBlock, caseDest),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action sur case atteignable et différente
        ∧ Cell(caseBlock) ∧ Near(caseMe, caseBlock) ∧ (caseBlock ≠ caseMe) 

        //Il y a un bloc sur la case
        ∧ BlockOn(caseBlock) 

        // La 3e case n'est pas une case
        ∧ ¬Cell(caseDest),

    EFFECT: 
        //Le monstre n'est plus sur la case monstre et n'existe plus
        ¬EnnemyOn(monstre, caseBlock)
)
``` 


### Pousser un bloc

Le personnage est situé sur une case valide, c'est-à-dire libre. Il se déplace vers un bloc donc il le pousse vers la 3e case dans la même direction et celle-ci est libre forcément (aucun autre cas possible)

```cpp
Action(
    PushBlock(caseMe, caseBlock, caseDest),

    PRECOND: 
        // Le joueur est sur une case valide (case vide)
        PlayerOn(caseMe) ∧ Cell(caseMe) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe)

        // Action demandée sur une case atteignable et différente
        ∧ Cell(caseBlock) ∧ Near(caseMe, caseBlock) ∧ (caseBlock ≠ caseMe) 

        //Il y a un bloc sur la case block
        ∧ BlockOn(caseBlock) 

        // La 3e case est libre
        ∧ Cell(caseDest) ∧ ¬KeyOn(caseMe) ∧ ¬CofferOn(caseMe) ∧ ¬EnnemyOn(caseMe) ∧ ¬BlockOn(caseMe),

    EFFECT: 
        //Le bloc n'est plus sur la case block mais sur la case dest
        ¬BlockOn(caseBlock) ∧ BlockOn(caseDest)
)
``` 

## Plan

```cpp
// Aller tout au bout à gauche
MovePayer(E7, E6)
MovePayer(E6, E5)
MovePayer(E5, E4)
MovePayer(E4, E3)
MovePayer(E3, E2)

//Aller tout en bas
MovePayer(E3, D2)
MovePayer(D2, C2)
MovePayer(C2, B2)
MovePayer(B2, A2)

// Aller tout au bout à gauche
MovePayer(A2, A1)
MovePayer(A1, A0)

// Récupérer la clé
MoveAndGetKey(A0, B0)

//Redescendre
MovePayer(B0, A0)

//Aller jsqu'au monstre à droite
MovePayer(A0, A1)
MovePayer(A1, A2)
MovePayer(A2, A3)
MovePayer(A3, A4)

//Pousser et Eliminer le monstre
PushMonster(A4, A5, A6)
MovePayer(A4, A5)
PushOnWallAndKillMonster(A5, A6, A7)
MovePayer(A5, A6)

//Remonter jusqu'au coffre
MovePayer(A6, B6)
MovePayer(B6, C6)
MovePayer(C6, D6)
MovePayer(D6, E6)

//Ouvrir le coffre en s'y déplaçant car on a la clé
MoveAndOpenCoffer(E6, F6)

//Atteindre la cible
MovePayer(F6, H6)

//GOAL : Target(H6) Player(me) On(me, H6) Cell(H6)
```


