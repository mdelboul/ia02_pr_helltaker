# IA02_PR_HellTaker
| information                       | détail                                |
|-----------------------------------|-------------------------------------- |
| projet de IA02                    | HellTaker (codes + rapport)           |
| Nom de l'équipe                   | Z Best Team                           |
|_Auteurs_                          | François Marechal, Maxime Delboulle   |
|_Date de création_                 | 13 mai 2022                           |

Ce projet vise à résoudre un problème de __planification__ (jeu HellTaker) qui généralise le jeu Sokoban.

Plusieurs approches ont été utilisées pour résoudre le problème le plus efficacement possible (en temps et en mémoire) :
- ASP (Answer Set Programming) ;
- recherche dans un espace d'états.

Le gain en performances est très corrélé au choix des heuristiques pour accélérer la recherche.

Le code s'exécute à partir de scripts Pÿthon.

Un rapport de quelques 20 pages documente le code.

## Architecture du fichier zip

### Répertoires

    .\codes : contient tous les codes.
    Les différentes méthodes sont dans :
    - .\codes\ASP
    - .\codes\Prolog
    - .\codes\rechercheEtat

### fichiers à la racine du dossier

    .\rapport_IA02_HellTaker.pdf
    .\readme.md 

## Exécutions des scripts

MAPS désigne un chemin vers une carte de format standard .txt (par exemple ./../../cartes/level1.txt)

Votre répertoire courant se trouve dans le dossier racine.

Pour la recherche d'états, exécuter le script suivant :

    - python .\rechercheEtat\helltaker_plan_recherche_etats_TP3B_Tr1.py MAPS

Pour la résolution ASP, exécuter le script suivant :

    - python .\ASP\helltaker_plan_ASP_TP3B_Tr1.py MAPS

Pour Prolog, il y a un fichier prolog pour les solutions de 1 à 7. Il faut les exécuter "à la main" :

    swipl
    consult("codes/Prolog/Modelisation_HellTaker_sol1.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol2.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol3.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol4.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol5.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol6.pl").
    consult("codes/Prolog/Modelisation_HellTaker_sol7.pl").
    actions(A), start(X), winning_plan_a(A, X, 50).

On a mis 50 (borne maximale de toutes les cartes, mais on peut l'initialiser au nombre de coups spécifique à la carte).

## Note

La résolution par ASP nécessite d'écrire dans un fichier ASP qui concatène un moteur générique et une instanciation. 

La vérification sous Prolog n'est malheureusement pas automatique.
