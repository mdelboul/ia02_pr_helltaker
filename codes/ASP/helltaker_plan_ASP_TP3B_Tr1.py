"""
Contenu : Résolution ASP et retour de la chaîne.
Command Line example :
python '.\ASP\helltaker_plan_ASP_TP3B_Tr1.py' '.\maps\level1.txt'
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 25 mai 2022
Date de dernière modification : 31 mai 2022
"""

from time import time
import sys, os
import clingo
from verificateur_HellTaker import verify

from instanciation_ASP import write_ASP_file

DEBUG = 0

def main (code_asp_path, filename) :
    """
    Appelle l'instanciation (écriture d'un fichier resolution.py)
    """

    if DEBUG:
        beginning = time() # calcul de temps

    # Ecriture du fichier clingo (prouveur + instanciation)
    resolution_asp_path = write_ASP_file(code_asp_path, filename)

    # Lecture du fichier clingo (prouveur + instanciation)
    with open(resolution_asp_path, encoding="utf-8") as resolution_asp :
        pb_to_solve = resolution_asp.read()

    # objet clingo pour la résolution : 1 seul modèle
    ctl = clingo.Control(["-n 1"])
    ctl.add("base", [], pb_to_solve)
    ctl.ground([("base", [])])

    result_asp = {}
    # résolution du problème
    resultat = ""
    with ctl.solve(yield_=True) as handle:
        # Lecture d'un modèle
        for model in handle:
            # Lecture des atomes do
            for atom in model.symbols(atoms=True):
                if atom.match("do", 2):
                    result_asp[atom.arguments[1].number] = atom.arguments[0].string
            # trie  des "do" en fonction du temps & stockage de la chaîne de caractère
            for _, action in sorted(result_asp.items(), key = lambda t : t[0]) :
                resultat += action

    if DEBUG :
        chrono = time() - beginning
        print ("temps en seconde : ", chrono) # calcul de temps

    if verify(resultat):
        print("[OK]")
        print(resultat.lower())
    else :
        print('[Pas de solution trouvée]')


    return resultat.lower() # retourne la chaîne de caractère en minuscule

if __name__ == "__main__":
    main(os.path.split(sys.argv[0])[0], sys.argv[1])
