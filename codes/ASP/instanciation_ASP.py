"""
Contenu : Instantiation des variables automatiquement à partir de la grille pour ASP
Command line example : python .\\codes\\ASP\\instanciation_ASP.py .\\codes\\maps\\level9.txt
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 2 juin 2022
Date de dernière modification : 7 juin 2022
"""

import sys
import os
from typing import List, Tuple, Callable
from helltaker_utils import grid_from_file

def write_ASP_file (code_asp_path, filename) :
    """
    Ecrit toutes les instances utiles pour la résolution du problème ASP
    Retourne le chemin du fichier ASP.
    """

    HEADER = \
"""%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Contenu : Méthode de réolution générique de HellTaker
% Convention : carte = matrice dont (0, 0) est le coin
%       en bas à gauche de l'écran.
% Auteurs : François Marechal, Maxime Delboulle
%       CODE REPRIS DE CELUI DE M. Belahcene Khaled
% Date de création : 31 mai 2022
% Date de dernière modification : 3 juin 2022
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%% INSTANCIATION
"""

    adjacent_cases  : Callable[[Tuple[int, int]], List[Tuple[int, int]]] = lambda position : \
        [
            (position[0]+1, position[1]),
            (position[0]-1, position[1]),
            (position[0]  , position[1]-1),
            (position[0]  , position[1]+1)
        ]
    adjacent_cases.__doc__ = """Renvoie une liste de 4 positions adjacentes"""

    infos = grid_from_file(filename)

    lines = infos['m']
    columns = infos['n']

    #Non-Fluents
    walls = "\n"+r"% Walls :"+"\n"
    girls = "\n"+r"% Girls :"+"\n"
    spikes = "\n"
    goals = "\n"+r"% Goals :"+"\n"

    #Fluents
    enemies = "\n"
    init_pos = "\n"
    keys = "\n"
    coffers = "\n"
    boxes = "\n"
    traps = "\n"
    key_counter = "\ninit(key_counter(0)).\n"
    action_counter = "\ninit(action_counter(tmax)).\n"

    prov_goals_list = []
    walls_list= []

    # Recherche d'objets sur la carte
    for line in range(lines) :
        for case in range(columns) :
            info_case = infos['grid'][line][case]
            case = (case, lines - line - 1) # représentation coin bas gauche == (0,0)
            if info_case == '#' :
                walls+=f"constant(walls{case}).\n"
                walls_list.append(case)
            if info_case == 'M' :
                enemies+=f"init(enemies{case}).\n"
            if info_case == 'B' :
                boxes+=f"init(boxes{case}).\n"
            if info_case == 'K' :
                keys+=f"init(keys{case}).\n"
            if info_case == 'L' :
                coffers+=f"init(coffers{case}).\n"
            if info_case == 'S' :
                spikes+=f"constant(spikes{case}).\n"
            if info_case == 'D' :
                for adjacent_case in adjacent_cases(case) :
                    prov_goals_list.append(adjacent_case)
                girls+=f"constant(girls{case}).\n"
            if info_case == 'H' :
                init_pos+=f"init(me{case}).\n"
            if info_case == 'T' :
                traps+=f"init(traps({case}, no_threat)).\n"
            if info_case == 'U' :
                traps+=f"init(traps({case}, threat)).\n"
            if info_case == 'O' :
                spikes+=f"constant(spikes{case}).\n"
                boxes+=f"init(boxes{case}).\n"
            if info_case == 'P' :
                traps+=f"init(traps({case}, no_threat)).\n"
                boxes+=f"init(boxes{case}).\n"
            if info_case == 'Q' :
                traps+=f"init(traps({case}, threat)).\n"
                boxes+=f"init(boxes{case}).\n"

    for goal_ in prov_goals_list :
        if goal_ not in walls_list :
            goals+=f"constant(goals{goal_}).\n"

    preproc_var = r"%% Constantes" + "\n" +\
    f"#const lines = {lines}.\n#const columns = {columns}.\n#const tmax = {infos['max_steps']}.\n"
    constant_var = "\n" + r"%% Partie Constante" + "\n" + \
        walls+girls+goals+spikes
    fluent_var = "\n" + r"%% Partie Fluente" + "\n" + \
        init_pos + boxes + enemies + traps + keys + coffers + action_counter + key_counter


    with open(os.path.join(code_asp_path, "resolution_" + infos["title"] + ".lp"),
                "w", encoding="utf-8") as final_file :
        final_file.writelines(HEADER)
        final_file.write(preproc_var)
        final_file.write(constant_var)
        final_file.write(fluent_var)
        with open(os.path.join(code_asp_path, "resolution.lp"),
                "r", encoding="utf-8") as engine_file:
            final_file.writelines(engine_file)

    return os.path.join(code_asp_path, "resolution_" + infos["title"] + ".lp")
