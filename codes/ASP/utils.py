"""
Contenu : Fonctions utilitaires pour les états, actions ...
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 26 mai 2022
Date de dernière modification : 31 mai 2022
"""

from typing import Tuple, List, Optional, Callable, FrozenSet, Dict
from helltaker_instanciation import map_rules, actions, State
from typing_helltaker import Action

def object_on_spike (
    position : Tuple[int, int],
    spikes_list : FrozenSet[Tuple[int, int]]) :
    """
    retourne vrai <=> la position se trouve sur des pics immobiles (spikes)
    """
    if not spikes_list : #si la liste de piques est vide
        return False
    if position in spikes_list :
        return True
    return False

def object_on_trap (
    position : Tuple[int, int],
    traps_list : FrozenSet[Tuple[Tuple[int, int], bool]]) :
    """
    retourne vrai <=> la position se trouve sur des pics mobiles (traps)
    """
    if not traps_list : #si la liste de traps est vide
        return False
    for trap in traps_list :
        if position in trap :
            return True
    return False

def object_on_threatening_trap (
    position : Tuple[int, int],
    traps_list : FrozenSet[Tuple[Tuple[int, int], bool]]) :
    """
    retourne vrai <=> la position se trouve sur des pics mobiles (traps) en position relevés
    """
    if not traps_list : # si la liste de traps est vide
        return False
    for trap in traps_list :
        if position in trap and trap[1] :
            return True
    return False

compute_action_counter : Callable[[Tuple[int, int], List[Tuple[int, int]]], int] = \
    lambda position, spikes_list, traps_list :\
    2 if (object_on_threatening_trap(position, traps_list) or
            object_on_spike(position, spikes_list)) else 1
compute_action_counter.__doc__ = """coût d'un mouvement. Renvoie 1 ou 2."""

goals : Callable[[State], bool] = \
    lambda state : state.me in map_rules['goals'] and state.action_counter >= 0
goals.__doc__ = """Renvoie vrai <=> but atteint avec un nombre de pas positif ou nul"""

succ : Callable[[State], Dict[State, Action]] = lambda state : \
    {do_fn(action, state) : action \
        for action in actions.values()\
            if do_fn(action, state)}
succ.__doc__ = """renvoie un dictionnaire dont la clef est l'état suivant et la valeur l'action."""

def one_step(position : Tuple[int, int], direction : str) :
    """ <=> relative_pos (Prolog) """
    i, j = position
    return {'r' : (i+1,j), 'l' : (i-1,j), 'u' : (i,j+1), 'd' : (i,j-1)}[direction]

free = lambda position : position not in map_rules['walls']
free.__doc__ =  """Renvoie vrai <=> la case est sur la carte (pas un mur)."""

conv_action_2_char : Callable[[Action], str] = \
    lambda action : action[1] if action[0] == "move" else action[1].upper()

def change_pos_traps (state : State) -> FrozenSet[Tuple[Tuple[int, int], bool]] :
    """
    change la position de chaque pic alternativement.
    """
    list_ = []
    for trap in state.traps :
        list_.append((trap[0], not trap[1]))
    return frozenset(list_)

def do_fn(action : Action, state : State) -> Optional[State] :
    """"
    Retourne l'état successeur de state par rapport à l'action.
    Retourne None si l'action n'est pas licite.
    """
    X0 = state.me
    X1 = one_step(X0, action.direction)
    boxes = state.boxes
    enemies = state.enemies
    spikes = state.spikes
    NewPosTraps = change_pos_traps(state)
    keys = state.keys
    coffer = state.coffer
    keycounter = state.keycounter
    traps = state.traps

    if action.verb == 'move' :
        if (X1 in map_rules['walls'] or X1 in enemies or X1 in boxes) :
            return None
        if (X1 not in keys and X1 not in coffer) : # movePlayer (STRIPS)
            return State(
                me=X1,
                boxes=frozenset(boxes),
                enemies=frozenset(enemies),
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys),
                coffer=frozenset(coffer),
                keycounter=keycounter,
                action_counter=state.action_counter-compute_action_counter(X1, spikes, NewPosTraps)
                )
        if (X1 in keys and X1 not in coffer) : # MoveAndGetKey (STRIPS)
            return State(
                me=X1,
                boxes=frozenset(boxes),
                enemies=frozenset(enemies),
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys - {X1}), # Get the key
                coffer=frozenset(coffer),
                keycounter=keycounter + 1, # Get the key (INCREMENT)
                action_counter=state.action_counter-compute_action_counter(X1, spikes, NewPosTraps)
                )
        if (keycounter > 0 and X1 in coffer and X1 not in keys) : # MoveAndOpenCoffer (STRIPS)
            return State(
                me=X1,
                boxes=frozenset(boxes),
                enemies=frozenset(enemies),
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys), 
                coffer=frozenset(coffer - {X1}), # Oppen the coffer
                keycounter=keycounter - 1, # Use the key (DECREMENT)
                action_counter=state.action_counter-compute_action_counter(X1, spikes, NewPosTraps)
                )
        return None
    if action.verb == 'push' :
        X2 = one_step(X1, action.direction)

        if (X1 in map_rules['walls'] or X2 in map_rules['girls'] or X1 in coffer) :
            return None
        if (X1 not in keys and X1 not in boxes and X1 in enemies) : # PushMonster
            # push & kill monster (STRIPS)
            if (X2 in coffer or X2 in enemies or X2 in boxes
                or X2 in map_rules['walls'] or object_on_spike(X2, spikes)
                or object_on_trap(X2, traps)) :
                return State(
                me=X0,
                boxes=frozenset(boxes),
                enemies=frozenset(enemies - {X1}), # monster killed
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys),
                coffer=frozenset(coffer),
                keycounter=keycounter,
                action_counter=state.action_counter-compute_action_counter(X0,spikes,NewPosTraps)
                )
            # else : PushMonster case vide (STRIPS)
            return State(
            me=X0,
            boxes=frozenset(boxes),
            enemies=frozenset({X2} | enemies - {X1}), # monster moved
            spikes=frozenset(spikes),
            traps=NewPosTraps,
            keys=frozenset(keys), 
            coffer=frozenset(coffer),
            keycounter=keycounter,
            action_counter = state.action_counter - compute_action_counter(X0, spikes, NewPosTraps)
            )
        if (X1 in boxes and X1 not in enemies) : # push boxes
            # push boxes with move only (STRIPS)
            if(X2 not in coffer and X2 not in boxes
                and X2 not in enemies and X2 not in map_rules['walls']) :
                return State(
                me=X0,
                boxes=frozenset({X2} | boxes - {X1}),
                enemies=frozenset(enemies),
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys), 
                coffer=frozenset(coffer),
                keycounter=keycounter,
                action_counter=state.action_counter-compute_action_counter(X0, spikes, NewPosTraps)
                )
            # else : push boxes without move (STRIPS)
            return State(
                me=X0,
                boxes=frozenset(boxes),
                enemies=frozenset(enemies),
                spikes=frozenset(spikes),
                traps=NewPosTraps,
                keys=frozenset(keys), 
                coffer=frozenset(coffer),
                keycounter=keycounter,
                action_counter=state.action_counter-compute_action_counter(X0, spikes, NewPosTraps)
                )
        return None
