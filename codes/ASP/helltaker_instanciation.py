"""
Contenu : Instantiation des variables automatiquement à partir de la grille.
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 26 mai 2022
Date de dernière modification : 31 mai 2022
"""

import sys
from typing import List, Tuple, Callable

from typing_helltaker import State, actions
from helltaker_utils import grid_from_file


adjacent_cases : Callable[[Tuple[int, int]], List[Tuple[int, int]]] = lambda position : \
    [
        (position[0]+1,position[1]),
        (position[0]-1,position[1]),
        (position[0]  ,position[1]-1),
        (position[0]  ,position[1]+1)
    ]
adjacent_cases.__doc__ = """Renvoie une liste de 4 positions adjacentes"""

filename = sys.argv[1]

# récupération de al grille et de toutes les infos
infos = grid_from_file(filename)

if infos["title"] == "Level 1" :
    ACTIONS_LIST = "ldLldLlLldlddRrUurrRrdr"
elif infos["title"] == "Level 2" :
    ACTIONS_LIST = "uurUuUurrrdrrDdDdlld"
elif infos["title"] == "Level 3" :
    ACTIONS_LIST = "lllllddddlludrrrrRrRruuuuuu"
elif infos["title"] == "Level 4" :
    ACTIONS_LIST = "dDdrDdrRruLluurDdrrrd"
elif infos["title"] == "Level 5" :
    ACTIONS_LIST = "dDddrrrrrUduuUulluuu"
elif infos["title"] == "Level 6" :
    ACTIONS_LIST = "lDrrDdLldLldDRrRrUudlldDdrRrUuRrRrDdLldd"
elif infos["title"] == "Level 7" :
    ACTIONS_LIST = "uUdllUuRdddllluuUuRrudRrRrUuRruu"
elif infos["title"] == "Level 8" :
    ACTIONS_LIST = "ruuuuuuuuuLl"
elif infos["title"] == "Level 9" :
    ACTIONS_LIST = "rUuRrRrdrRluUuRrRrDdRrUurllluUulu"
else :
    ACTIONS_LIST = False

lines = infos['m']
columns = infos['n']

walls_list = []
boxes_list = []
spikes_list = []
enemies_list = []
init_pos = []
keys_list = []
coffers_list = []
prov_goals_list = []
girls_list = []
traps_list = []

for line in range(lines) :
    for column in range(columns) :
        info_case = infos['grid'][line][column]
        case = (column, lines - line - 1) # pour obtenir la représentation coin bas gauche == (0,0)
        if info_case == '#' :
            walls_list.append(case)
        if info_case == 'M' :
            enemies_list.append(case)
        if info_case == 'B' :
            boxes_list.append(case)
        if info_case == 'K' :
            keys_list.append(case)
        if info_case == 'L' :
            coffers_list.append(case)
        if info_case == 'S' :
            spikes_list.append(case)
        if info_case == 'D' :
            for adjacent_case in adjacent_cases(case) :
                prov_goals_list.append(adjacent_case)
            girls_list.append(case)
        if info_case == 'H' :
            init_pos = case
        if info_case == 'T' :
            traps_list.append((case, False)) # non-dangerous
        if info_case == 'U' :
            traps_list.append((case, True)) # dangerous
        if info_case == 'O' :
            spikes_list.append(case)
            boxes_list.append(case)
        if info_case == 'P' :
            traps_list.append((case, False)) # non-dangerous
            boxes_list.append(case)
        if info_case == 'Q' :
            traps_list.append((case, True)) # dangerous
            boxes_list.append(case)

goals_list = []
for goal_ in prov_goals_list :
    if not goal_ in walls_list :
        goals_list.append(goal_)

INIT_KEY_COUNTER = 0 # Default

s0 = State(
    me=init_pos,
    boxes=frozenset(boxes_list),
    enemies=frozenset(enemies_list),
    spikes=frozenset(spikes_list),
    keys=frozenset(keys_list),
    coffer=frozenset(coffers_list),
    traps=frozenset(traps_list),
    keycounter=INIT_KEY_COUNTER,
    action_counter = infos['max_steps']
    )

map_rules = {
    'goals': set (goals_list),
    'girls': set (girls_list),
    'walls': set (walls_list),
    'actions': actions
}
