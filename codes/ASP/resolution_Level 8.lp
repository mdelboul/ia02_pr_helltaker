%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Contenu : Méthode de réolution générique de HellTaker
% Convention : carte = matrice dont (0, 0) est le coin
%       en bas à gauche de l'écran.
% Auteurs : François Marechal, Maxime Delboulle
%       CODE REPRIS DE CELUI DE M. Belahcene Khaled
% Date de création : 31 mai 2022
% Date de dernière modification : 3 juin 2022
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%% INSTANCIATION
%% Constantes
#const lines = 14.
#const columns = 9.
#const tmax = 12.

%% Partie Constante

% Walls :
constant(walls(0, 13)).
constant(walls(1, 13)).
constant(walls(2, 13)).
constant(walls(3, 13)).
constant(walls(4, 13)).
constant(walls(5, 13)).
constant(walls(6, 13)).
constant(walls(7, 13)).
constant(walls(8, 13)).
constant(walls(0, 12)).
constant(walls(1, 12)).
constant(walls(2, 12)).
constant(walls(3, 12)).
constant(walls(5, 12)).
constant(walls(6, 12)).
constant(walls(7, 12)).
constant(walls(8, 12)).
constant(walls(0, 11)).
constant(walls(8, 11)).
constant(walls(0, 10)).
constant(walls(8, 10)).
constant(walls(0, 9)).
constant(walls(2, 9)).
constant(walls(6, 9)).
constant(walls(8, 9)).
constant(walls(0, 8)).
constant(walls(8, 8)).
constant(walls(0, 7)).
constant(walls(2, 7)).
constant(walls(6, 7)).
constant(walls(8, 7)).
constant(walls(0, 6)).
constant(walls(8, 6)).
constant(walls(0, 5)).
constant(walls(2, 5)).
constant(walls(6, 5)).
constant(walls(8, 5)).
constant(walls(0, 4)).
constant(walls(8, 4)).
constant(walls(0, 3)).
constant(walls(2, 3)).
constant(walls(6, 3)).
constant(walls(8, 3)).
constant(walls(0, 2)).
constant(walls(8, 2)).
constant(walls(0, 1)).
constant(walls(1, 1)).
constant(walls(2, 1)).
constant(walls(6, 1)).
constant(walls(7, 1)).
constant(walls(8, 1)).
constant(walls(0, 0)).
constant(walls(1, 0)).
constant(walls(2, 0)).
constant(walls(3, 0)).
constant(walls(4, 0)).
constant(walls(5, 0)).
constant(walls(6, 0)).
constant(walls(7, 0)).
constant(walls(8, 0)).

% Girls :
constant(girls(4, 12)).

% Goals :
constant(goals(4, 11)).


%% Partie Fluente

init(me(4, 2)).


init(enemies(4, 11)).
init(enemies(4, 10)).
init(enemies(4, 9)).
init(enemies(4, 8)).
init(enemies(4, 7)).
init(enemies(4, 6)).
init(enemies(4, 5)).
init(enemies(4, 4)).
init(enemies(4, 3)).




init(action_counter(tmax)).

init(key_counter(0)).

%%%%%%%%%% Résolution

%%% Fluent
% Fluent initialisé.
fluent(F, 0) :- init(F).

% fluents restent les mêmes à moins qu'ils ne soient modifiés : retirés, incrémentés, décrémentés
fluent(F, T + 1) :- 
    fluent(F, T), 
    instant(T + 1),
    not removed(F, T).

%%% description du temps
instant(0..tmax).
abscissa(0..columns).
ordinate(0..lines).

%%% Actions possibles
direction(left ; right ; up ; down).
verb(move ; push ; nothing).
action(V, D) :- verb(V), direction(D).

%%% Conversion de l'action en string (affichage du résultat compact)
char_2_action(CHAR, V, D) :- CHAR = "d", verb(V), V = move, direction(D), D = down.
char_2_action(CHAR, V, D) :- CHAR = "l", verb(V), V = move, direction(D), D = left.
char_2_action(CHAR, V, D) :- CHAR = "r", verb(V), V = move, direction(D), D = right.
char_2_action(CHAR, V, D) :- CHAR = "u", verb(V), V = move, direction(D), D = up.
char_2_action(CHAR, V, D) :- CHAR = "D", verb(V), V = push, direction(D), D = down.
char_2_action(CHAR, V, D) :- CHAR = "L", verb(V), V = push, direction(D), D = left.
char_2_action(CHAR, V, D) :- CHAR = "R", verb(V), V = push, direction(D), D = right.
char_2_action(CHAR, V, D) :- CHAR = "U", verb(V), V = push, direction(D), D = up.
char_2_action(CHAR, V, D) :- CHAR = "", verb(V), V = nothing, direction(D), D = left.

%%% On simplifie : mouvement inutile <=> do_3(nothing, left, T)
:- do_3 (nothing, right, T).
:- do_3 (nothing, up, T).
:- do_3 (nothing, down, T).

%%% Relative pos
relative_pos(up, (X,Y), (X,Y1)) :-
	Y1 = Y + 1,
	abscissa(X),
    ordinate(Y).
relative_pos(down, (X,Y), (X,Y1)) :-
	Y1 = Y - 1,
	abscissa(X),
    ordinate(Y).
relative_pos(left, (X,Y), (X1,Y)) :-
	X1 = X - 1,
	abscissa(X),
    ordinate(Y).
relative_pos(right, (X,Y), (X1,Y)) :-
	X1 = X + 1,
	abscissa(X),
    ordinate(Y).

% Générateur d'action (do/2 est plus compact)
{ do(Act, T) : char_2_action(Act, V, D), action(V, D) } = 1 :- instant(T), instant(T+1).

%% do/2 <=> do/3
% do/2 => do/3
do_3 (V, D, T) :-
    char_2_action(CHAR, V, D),
    do(CHAR, T).

% not do/2 => not do/3
:-  do(CHAR, T),
    not char_2_action(CHAR, V, D),
    do_3 (V, D, T).

%%% move sur case libre
%% PRE génériques
% PRE : pas de mur
:-  do_3 (move, D, T),
    fluent(me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    constant (walls(X1,Y1)).

% PRE : pas d'ennemis
:-  do_3 (move, D, T),
    fluent(me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    fluent(enemies(X1,Y1), T).

% PRE : pas de boîtes
:-  do_3 (move, D, T),
    fluent(me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    fluent(boxes(X1,Y1), T).

%% POST générique
% POST : le joueur bouge d'une case dans la bonne direction
fluent(me(X1, Y1), T+1) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)).

removed(me(X, Y), T) :-
    do_3 (move, _, T),
    fluent(me(X, Y), T).

%%% move : le joueur prend la clef pour ouvrir un coffre
% Pas de PRE supplémentaires

% POST : clef retirée
removed(keys(X1, Y1), T+1) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (keys(X1,Y1), T),
    relative_pos (D, (X,Y), (X1, Y1)).

% POST : on retire l'ancien fluent (compteur obsolète)
removed(key_counter(K), T) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (keys(X1,Y1), T),
    fluent(key_counter(K), T),
    relative_pos (D, (X,Y), (X1, Y1)).

% POST : ajout du nouveau fluent (compteur incrémentée)
fluent(key_counter(K+1), T+1) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (keys(X1,Y1), T),
    fluent(key_counter(K), T),
    relative_pos (D, (X,Y), (X1, Y1)).

%%% move : le joueur utilise la clef pour ouvrir un coffre
% PRE : il faut une clef au moins
:-  do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (key_counter(K), T),
    fluent (coffers(X1,Y1), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    K <= 0.

% POST : coffre retirée
removed(coffers(X1, Y1), T+1) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (key_counter(K), T),
    K > 0,
    fluent (coffers(X1,Y1), T),
    relative_pos (D, (X,Y), (X1, Y1)).

% POST : on retire l'ancien fluent (compteur obsolète)
removed(key_counter(K), T) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (key_counter(K), T),
    K > 0,
    fluent (coffers(X1,Y1), T),
    relative_pos (D, (X,Y), (X1, Y1)).

% POST : ajout du nouveau fluent (compteur décrémentée)
fluent(key_counter(K-1), T+1) :-
    do_3 (move, D, T),
    fluent (me(X, Y), T),
    fluent (key_counter(K), T),
    K > 0,
    fluent (coffers(X1,Y1), T),
    relative_pos (D, (X,Y), (X1, Y1)).

%%% push générique
%% PRE génériques : on pousse un monstre ou une boîte

% on pousse une boîte ou un monstre
:-  do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    not fluent(enemies(X1, Y1), T) ;
    not fluent(boxes(X1, Y1), T).

% On ne pousse pas d'objet sur la démone
:-  do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    relative_pos (D, (X1, Y1), (X2, Y2)),
    constant (girls(X2, Y2)).

%% POST générique : on retire le fluent de monstre
removed(enemies(X1, Y1), T) :-
    do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    fluent(enemies(X1, Y1), T).

%% POST générique : on retire le fluent de boîte si la boîte bouge
removed(boxes(X1, Y1), T) :-
    do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    relative_pos (D, (X1, Y1), (X2, Y2)),
    fluent(boxes(X1, Y1), T),
    not fluent(boxes(X2, Y2), T),
    not fluent(enemies(X2, Y2), T),
    not fluent(coffers(X2, Y2), T),
    not constant(walls(X2, Y2)).

%%% push monster
%% POST : push without killing monster
fluent(enemies(X2, Y2), T+1) :-
    do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    relative_pos (D, (X1, Y1), (X2, Y2)),
    fluent(enemies(X1, Y1), T),
    not fluent(boxes(X2, Y2), T),
    not fluent(enemies(X2, Y2), T),
    not fluent(coffers(X2, Y2), T),
    not fluent(traps((X2, Y2), _), T),
    not constant (spikes(X2, Y2)),
    not constant(walls(X2, Y2)).

%% POST : push box with move
fluent(boxes(X2, Y2), T+1) :-
    do_3 (push, D, T),
    fluent (me(X, Y), T),
    relative_pos (D, (X,Y), (X1, Y1)),
    relative_pos (D, (X1, Y1), (X2, Y2)),
    fluent(boxes(X1, Y1), T),
    not fluent(boxes(X2, Y2), T),
    not fluent(enemies(X2, Y2), T),
    not fluent(coffers(X2, Y2), T),
    not constant(walls(X2, Y2)).

%%% Gestion des pièges
threatening(threat ; no_threat).

% Définition de traps.
:-  fluent(traps((_ , _), THREAT),T),
    not threatening(THREAT).

%%% bloque la propagation des pièges
removed(traps((X, Y), THREAT), T) :-
    fluent(traps((X, Y), THREAT), T).

% permutation des pièges
fluent(traps((X,Y), no_threat), T+1) :-
    instant(T),
    instant(T+1),
    fluent(traps((X,Y), threat), T).
fluent(traps((X,Y), threat), T+1) :-
    instant(T),
    instant(T+1),
    fluent(traps((X,Y), no_threat), T).

%%% Bloque les actions si héros à la fin.
% Si but atteint => prochaine action est l'action vide
do_3 (nothing, left, T+1) :-
    fluent (me(X, Y), T),
    constant (goals(X, Y)).

% action vide à T => action vide à T+1
:-  do_3 (nothing, left, T),
    instant(T),
    not do_3 (nothing, left, T+1).

%%% action_counter
% coût classique
fluent(action_counter(COUNTER-1), T+1) :-
    fluent(action_counter(COUNTER), T),
    fluent (me(X, Y), T+1),
    not constant (spikes (X, Y)),
    not do_3 (nothing, left, T),
    not fluent (traps((X, Y), threat), T+1).

% coût sur spike
fluent(action_counter(COUNTER-2), T+1) :-
    fluent (action_counter(COUNTER), T),
    fluent (me(X, Y), T+1),
    not do_3 (nothing, left, T),
    constant (spikes (X, Y)).

% coût sur trap
fluent(action_counter(COUNTER-2), T+1) :-
    fluent (action_counter(COUNTER), T),
    fluent (me(X, Y), T+1),
    not do_3 (nothing, left, T),
    fluent (traps((X, Y), threat), T+1).

% Faire patienter le joueur (action vide) ne coûte rien.
fluent(action_counter(COUNTER), T+1) :-
    fluent (action_counter(COUNTER), T),
    do_3 (nothing, left, T).

% Retire l'ancienne valeur
removed (action_counter(COUNTER), T) :-
    fluent (action_counter(COUNTER), T).

%%% Goal
% Arrêt de la recherche si compteur de pas négatif
:-  fluent(me(X,Y), T),
    fluent(action_counter(COUNTER), T),
    COUNTER < 0.

% Goal
:-  fluent(me(X, Y), tmax),
    not constant(goals(X, Y)).

#show do/2.
