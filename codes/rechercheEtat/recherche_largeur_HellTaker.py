"""
Contenu : modélisation de la recherche d'états (largeur) en Python pour HellTaker
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 25 mai 2022
Date de dernière modification : 31 mai 2022
"""

from typing import Callable, Tuple, List, Dict
from typing_helltaker import Action, State
from helltaker_instanciation import map_rules

forbidden_moves : Callable[[Action, Action], bool] = lambda action1 , action2 : \
    (
        action1 == Action("move", "u") and action2 == Action("move", "d")  or
        action1 == Action("move", "d") and action2 == Action("move", "u") or
        action1 == Action("move", "l") and action2 == Action("move", "r") or
        action1 == Action("move", "r") and action2 == Action("move", "l") or
        action1 == Action("push", "u") and action2 == action1 or
        action1 == Action("push", "d") and action2 == action1 or
        action1 == Action("push", "l") and action2 == action1 or
        action1 == Action("push", "r") and action2 == action1
    )
forbidden_moves.__doc__ = """renvoie vrai si et seulement si les mouvements ne sont pas utiles"""

# Recherche dans l'espace d'états

distance : Callable[[Tuple[int, int], Tuple[int, int]], int] = lambda pos1, pos2 : \
    abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])
distance.__doc__ = """renvoie la distance de Manhattan entre deux positions."""

def distance_list (
    pos1 : Tuple[int, int],
    pos_list : List[Tuple[int, int]]) :
    """ retourne la distance de Manhattan minimale entre une position et une liste de positions """
    return (min([distance(pos1, pos2) for pos2 in pos_list]))

def search_with_parent(
    initial_state : State,
    goals : Callable[[State], bool],
    succ : Callable[[State], Dict[State, Action]],
    remove,
    insert,
    debug=True) :
    """"
    recherche dans un espace d'état avec mémorisation
    """
    successors_stack = [(initial_state, None)]
    save = {initial_state: None}
    current_state = initial_state
    while successors_stack:
        if debug:
            print("l =", successors_stack)
        element, successors_stack = remove(successors_stack)
        current_state, old_action = element
        for curr_succ, action in succ(current_state).items() :
            if curr_succ not in save and \
                curr_succ.action_counter >= distance_list(curr_succ.me, map_rules["goals"]) and \
                not forbidden_moves(old_action, action) and curr_succ.action_counter >=0  :
                save[curr_succ] = (current_state, action)
                if goals(curr_succ):
                    return curr_succ, save
                # on peut revenir sur ses pas uniquement si ça a une utilité (récupérer la clef)
                if curr_succ.keycounter == current_state.keycounter :
                    insert((curr_succ, action), successors_stack)
                else :
                    insert((curr_succ, None), successors_stack)
    return None, save

def insert_tail(element, list_ : List) ->  List :
    """
    Ajoute l'élément en queue.
    """
    list_.append(element)
    return list_

def remove_head(list_ : List) -> Tuple[any, List]:
    """
    Retire l'élément en tête.
    """
    return list_.pop(0), list_

def remove_tail(list_ : List) -> Tuple[any, List] :
    """
    Retire l'élément en queue.
    """
    return list_.pop(), list_

def dict2path (state : State, dict_ : Dict[State, State]) :
    """
    Algorithme du petit poucet. Construit la chaîne de d'états à partir d'un état final.
    """
    list_ = [(state, None)]
    while dict_[state] is not None:
        parent, action = dict_[state]
        list_.append((parent, action))
        state = parent
    list_.reverse()
    return list_
