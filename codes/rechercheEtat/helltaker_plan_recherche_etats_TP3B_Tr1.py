"""
Contenu : Vérification du plan + recherche en largeur à partir d'une instanciation automatique.
Command Line example :
python '.\rechercheEtat\helltaker_plan_recherche_etats_TP3B_Tr1.py' '.\maps\level1.txt'
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 25 mai 2022
Date de dernière modification : 31 mai 2022
"""

from time import time

from recherche_largeur_HellTaker import search_with_parent, remove_head, insert_tail, dict2path
from verificateur_HellTaker import verify
from utils import goals, succ, conv_action_2_char

from helltaker_instanciation import  s0, ACTIONS_LIST

DEBUG = 0

def main () :
    """
    Deux modes : Debug == 1 ou Debug == 0
    La fonction main prend 1 argument qui est le chemin du fichier de carte.
    Le script utilise helltaker_instanciation.py qui instancie les variables
    utiles pour représenter un état

    En mode hors debug (debug == 0), la fonction fait
    une recherche en largeur spéciale (cf. recherche_largeur_HellTaker.py).
    Il retourne une chaîne contenant les actions à
    faire pour placer le héros à côté des cases démones.

    En mode debug (debug == 1), on affiche la vérification (cf. verificateur_HellTaker.py),
    la suite d'états, le chronomètre sur la recherche de la recherche et si le résultat est valide.
    """
    if DEBUG:
        if ACTIONS_LIST:
            print (verify(ACTIONS_LIST))
        beginning = time() # calcul de temps

    save = {}

    s_end, save = search_with_parent(s0, goals, succ, remove_head, insert_tail, debug=False)

    if DEBUG :
        chrono = time() - beginning

        print ("temps en seconde : ", chrono) # calcul de temps
        print ("nombre d'états explorés : ", len(save))
        chaine = "trouve" if s_end is not None and goals(s_end) else "pas trouve" 
        print(chaine)

    resultat = ""
    if s_end is not None : # Solution trouvée
        for state_ in dict2path(s_end, save)[:-1] :
            resultat += conv_action_2_char(state_[-1])
        resultat = resultat.lower()
        print("[OK]")
        print(resultat)
    else :
        print('[Pas de solution trouvée]')

if __name__ == "__main__":
    main()
