"""
Contenu : fonctions de typage utiles pour HellTaker
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 26 mai 2022
Date de dernière modification : 31 mai 2022
"""

from collections import namedtuple

# TYPING

Action = namedtuple('action',('verb','direction'))

actions = {d : Action('move', d) for d in 'udrl'} | {d.upper() : Action('push', d) for d in 'udrl'}

State = namedtuple('state',
    ('me', 'boxes', 'enemies', 'spikes', 'keys', 'coffer', 'keycounter', 'action_counter', 'traps'))
