"""
Contenu : modélisation d'un vérificateur en Python pour HellTaker
Convention : carte = matrice dont (0, 0) est le coin en bas à gauche de l'écran.
Auteurs : François Marechal, Maxime Delboulle
      CODE REPRIS DE CELUI DE M. Belahcene Khaled
Date de création : 25 mai 2022
Date de dernière modification : 31 mai 2022
"""

from helltaker_instanciation import s0
from utils import actions, do_fn, goals

def verify (liste_of_actions : str) -> bool :
    """
    Vérifie si la suite d'actions permet d'atteindre la démone.
    """
    state = s0
    save = {}

    for a in liste_of_actions :
        action = actions[a]
        newstate = do_fn(action, state)
        save[state] = newstate
        state = newstate

    return goals(state)
