%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Contenu : modélisation complète d'un vérificateur de plan en Prolog pour HellTaker
% Convention : carte = matrice dont (0,0) est le coin en bas à gauche de l'écran.
% Auteurs : François Marechal, Maxime Delboulle
% Date de création : 10 mai 2022
% Date de dernière modification : 17 mai 2022
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% SEQUENCE D'ACTION : 'lllllddddlludrrrrRrRruuuuuu'

% MAP de la solution 3
case(0, 0).
case(0, 1).
case(1, 0).
case(2, 0).
case(2, 1).
case(2, 2).
case(2, 3).
case(2, 4).
case(3, 0).
case(3, 2).
case(3, 4).
case(4, 0).
case(4, 1).
case(4, 2).
case(4, 3).
case(4, 4).
case(5, 0).
case(5, 2).
case(5, 4).
case(6, 0).
case(6, 1).
case(6, 2).
case(6, 3).
case(6, 4).
case(6, 5).
case(6, 6).
case(7, 2).
case(7, 3).
case(7, 4).

% INITIATES GOALS & POSITIONS
start(
	state(
		me(7,4),
		boxes([]),
		enemies([pos(5, 0), pos(4, 2)]),
		pinnacle([[pos(2, 1), threat(true)], [pos(2, 3), threat(true)], [pos(3, 4), threat(true)], [pos(4, 1), threat(true)], [pos(4, 3), threat(true)], [pos(4, 4), threat(true)], [pos(5, 2), threat(true)], [pos(6, 2), threat(true)]]),
		keys([pos(0, 1)]),
		coffer([pos(6,5)]),
		key_counter(0)
		)
	).
target(
	state(
		me(6, 6),
		boxes(_),
		enemies(_),
		pinnacle(_),
		keys(_),
		coffer(_),
		key_counter(_)
		)
	).

%% Choose the mode of the changing position pinnacles.
% change_pos_pinnacles_method("alternatively").
change_pos_pinnacles_method("permanent").

actions([
	action("move", "left"),
	action("move", "left"),
	action("move", "left"),
	action("move", "left"),
	action("move", "left"),
	action("move", "down"),
	action("move", "down"),
	action("move", "down"),
	action("move", "down"),
	action("move", "left"),
	action("move", "left"),
	action("move", "up"),
	action("move", "down"),
	action("move", "right"),
	action("move", "right"),
	action("move", "right"),
	action("move", "right"),
	action("push", "right"),
	action("move", "right"),
	action("push", "right"),
	action("move", "right"),
	action("move", "up"),
	action("move", "up"),
	action("move", "up"),
	action("move", "up"),
	action("move", "up"),
	action("move", "up")
]).

% utils
notin(_, []).
notin(X, [Y|R]) :- dif(X,Y), notin(X,R).

in_pinnacle_threatening(_, []) :- false.
in_pinnacle_threatening(Pos1, [[Pos2, threat(BOOL)]|_]) :- Pos1 = Pos2, BOOL = true.
in_pinnacle_threatening(Pos1, [[Pos2, threat(BOOL)]|R]) :- (dif(Pos1, Pos2) ; BOOL = false), in_pinnacle_threatening(Pos1,R).

in_pinnacle(_, []) :- false.
in_pinnacle(Pos1, [[Pos2, threat(_)]|R]) :- Pos1 = Pos2 ; in_pinnacle(Pos1,R).

found_remove(X, [X|R], R).
found_remove(X, [T|R], [T|R1]) :-
	dif(X, T),
	found_remove(X,R,R1).

equal_lists(L1, L2) :- %% EQUAL SETS
	forall( 
    	member(Var, L1),
    	member(Var, L2)
	),
	forall( 
    	member(Var, L2),
    	member(Var, L1)
	).

equal_states(state(me(X,Y), boxes(_), enemies(_), pinnacle(_), keys(_), coffer(_), key_counter(_)), state(me(X,Y), boxes(_), enemies(_), pinnacle(_), keys(_), coffer(_), key_counter(_))).

ends(X, [X]).
ends(X, [_|R]) :- ends(X, R).

extract_first_elements(0, _, []).
extract_first_elements(N, [T|R], Out) :-
	N1 is N - 1,
	extract_first_elements(N1, R, Out2),
	Out = [T|Out2].

% State machines
relative_pos(D, pos(X,Y), pos(X,Y1)) :-
	Y1 is Y + 1,
	case(X,Y),
	case(X,Y1),
	D = "up".
relative_pos(D, pos(X,Y), pos(X,Y1)) :-
	Y1 is Y - 1,
	case(X,Y),
	case(X,Y1),
	D = "down".
relative_pos(D, pos(X,Y), pos(X1,Y)) :-
	X1 is X - 1,
	case(X,Y),
	case(X1,Y),
	D = "left".
relative_pos(D, pos(X,Y), pos(X1,Y)) :-
	X1 is X + 1,
	case(X,Y),
	case(X1,Y),
	D = "right".

% Actions

%% change alternatively the threatening positions of the pinnacles
change_pos_pinnacles([], []).
change_pos_pinnacles([[Pos2, threat(BOOL1)]|R1], [[Pos2, threat(BOOL2)]|R2]) :-
	(
		(BOOL1 = true, BOOL2 = false) ;
		(BOOL1 = false, BOOL2 = true)
	),
	change_pos_pinnacles(R1, R2),
	change_pos_pinnacles_method("alternatively").
change_pos_pinnacles([[Pos2, threat(BOOL1)]|R1], [[Pos2, threat(BOOL2)]|R2]) :-
	(
		(BOOL1 = true, BOOL2 = true) ;
		(BOOL1 = false, BOOL2 = false)
	),
	change_pos_pinnacles(R1, R2),
	change_pos_pinnacles_method("permanent").

%% Move Action (on free case with no coffer, key)
do(action("move", Direction), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L3), keys(L4), coffer(L5), key_counter(L6)), state(me(X1,Y1), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(L6))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	notin(pos(X1, Y1), L),
	notin(pos(X1, Y1), L2),
	change_pos_pinnacles(L3, L9),
	notin(pos(X1, Y1), L4),
	notin(pos(X1, Y1), L5).
%% Move Action (on key position) : get the key & increment the key_counter
do(action("move", Direction), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X1,Y1), boxes(L), enemies(L2), pinnacle(L9), keys(L7), coffer(L5), key_counter(Counterpp))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	notin(pos(X1, Y1), L),
	notin(pos(X1, Y1), L2),
	notin(pos(X1, Y1), L5),
	member(pos(X1, Y1), L4),
	change_pos_pinnacles(L3, L9),
	found_remove(pos(X1, Y1), L4, L7), %%%% get the key
	Counterpp is Counter + 1. %%%% increment the key_counter
%% Move Action (on coffer position) : use the key (key_counter > 0) & decrement the key_counter
do(action("move", Direction), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X1,Y1), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L6), key_counter(Countermm))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	notin(pos(X1, Y1), L),
	notin(pos(X1, Y1), L2),
	notin(pos(X1, Y1), L4),
	member(pos(X1, Y1), L5),
	change_pos_pinnacles(L3, L9),
	Counter > 0, %%%% (key_counter > 0)
	found_remove(pos(X1, Y1), L5, L6), %%%% use the key
	Countermm is Counter - 1. %%%% decrement the key_counter

%% Push Action for boxes only (Unlike Sokoban, character does not move.)
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes([pos(X2,Y2)|L1]), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	relative_pos(Direction, pos(X1,Y1), pos(X2, Y2)),
	found_remove(pos(X1, Y1), L, L1),
	change_pos_pinnacles(L3, L9),
	notin(pos(X2, Y2), L),
	notin(pos(X2, Y2), L2).

%% Push Action for boxes only. Box is blocked by the borders of the map. (Unlike Sokoban, character does not move.).
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	\+relative_pos(Direction, pos(X1,Y1), pos(_, _)),
	change_pos_pinnacles(L3, L9),
	member(pos(X1, Y1), L).

%% Push Action for enemies only without elimination
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L1), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies([pos(X2,Y2)|L2]), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	relative_pos(Direction, pos(X1,Y1), pos(X2,Y2)),
	found_remove(pos(X1, Y1), L1, L2),
	change_pos_pinnacles(L3, L9),
	notin(pos(X2, Y2), L),
	notin(pos(X2, Y2), L1),
	notin(pos(X2, Y2), L1),
	notin(pos(X2, Y2), L4),
	notin(pos(X2, Y2), L5),
	notin(pos(X2, Y2), L9).

%% Push Action for enemies only with elimination (enemy cannot go out of the map nor take the pos of another object (enemy or box or pinnacle))
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L1), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	change_pos_pinnacles(L3, L9),
	found_remove(pos(X1, Y1), L1, L2),
	\+relative_pos(Direction, pos(X1,Y1), pos(_,_)).
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L1), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	relative_pos(Direction, pos(X1,Y1), pos(X2,Y2)),
	change_pos_pinnacles(L3, L9),
	found_remove(pos(X1, Y1), L1, L2),
	member(pos(X2, Y2), L).
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L1), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	relative_pos(Direction, pos(X1,Y1), pos(X2,Y2)),
	change_pos_pinnacles(L3, L9),
	found_remove(pos(X1, Y1), L1, L2),
	member(pos(X2, Y2), L1).
do(action("push", Direction), state(me(X,Y), boxes(L), enemies(L1), pinnacle(L3), keys(L4), coffer(L5), key_counter(Counter)), state(me(X,Y), boxes(L), enemies(L2), pinnacle(L9), keys(L4), coffer(L5), key_counter(Counter))) :-
	relative_pos(Direction, pos(X,Y), pos(X1,Y1)),
	relative_pos(Direction, pos(X1,Y1), pos(X2,Y2)),
	change_pos_pinnacles(L3, L9),
	found_remove(pos(X1, Y1), L1, L2),
	in_pinnacle(pos(X2, Y2), L9).

% generic plan
plan([]).
plan([A|T]) :- action(A), plan(T).

% valid generic plan
valid_plan([], [_], Counter) :- Counter >= 0.
%% COST == 1 <=> position of the character not in a threatening pinnacle position
valid_plan([A|AT], [S1, S2|ST], N) :-
	N > 0,
	N1 is N-1,
	do(A, S1, S2),
	S2 = state(me(X,Y), boxes(_), enemies(_), pinnacle(L), keys(_), coffer(_), key_counter(_)),
	\+in_pinnacle_threatening(pos(X, Y), L),
	valid_plan(AT, [S2|ST], N1).
%% COST == 2 <=> position of the character in a threatening pinnacle position
valid_plan([A|AT], [S1, S2|ST], N) :-
	N > 0,
	N1 is N-2,
	do(A, S1, S2),
	S2 = state(me(X,Y), boxes(_), enemies(_), pinnacle(L), keys(_), coffer(_), key_counter(_)),
	in_pinnacle_threatening(pos(X, Y), L),
	valid_plan(AT, [S2|ST], N1).

% generate Successive states thanks to initiate_state & Actions[]
generate_states_by_actions([], Initiate_state, [Initiate_state]).
generate_states_by_actions([A|R], State, State_liste) :-
	length([A|R], Longueur),
	Longueur > 0,
	do(A, State, Next_state),
	generate_states_by_actions(R, Next_state, State_liste2),
	State_liste = [State|State_liste2].

% specific winning plan
winning_plan(A, E, N) :-
	valid_plan(A, E, N),
	last(E, Last_state),
	target(Y),
	equal_states(Last_state, Y).

% specific winning plan just by using actions
winning_plan_a(A, Initiate_state, N) :-
	generate_states_by_actions(A, Initiate_state, State_liste),
	winning_plan(A, State_liste, N).

%% REQUESTS
%
% consult("codes/Prolog/Modelisation_HellTaker_sol3.pl").

% actions(A), start(X).
% 	%% Useful requests for debugging
	
% 	actions(A), start(X), generate_states_by_actions(A, X, Liste).
	
% 	actions(A), start(X), [T|R] = A, generate_states_by_actions([T], X, Liste).
	
% 	actions(A), start(X), extract_first_elements(23, A, SubA),  generate_states_by_actions(SubA, X, Liste).

% actions(A), start(X), winning_plan_a(A, X, 32).
